#!/usr/bin/env python

import argparse
from Bio import SeqIO, File
import numpy as np
import re

import time, sys
# update_progress() : Displays or updates a console progress bar
## Accepts a float between 0 and 1. Any int will be converted to a float.
## A value under 0 represents a 'halt'.
## A value at 1 or bigger represents 100%
def update_progress(progress):
    barLength = 60 # Modify this to change the length of the progress bar
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(barLength*progress))
    text = "\rProgress: [{0}] {1:.2f}% {2}".format( "#"*block + "-"*(barLength-block), progress*100, status)
    sys.stdout.write(text)
    sys.stdout.flush()


parser = argparse.ArgumentParser()
parser.add_argument("file",  help="fastq file to analyze for length distribution")
#parser.add_argument("length",  help="Length of the reads.", type=int)
parser.add_argument("--progress", help="Display a progress bar.", action='store_true')
#parser.add_argument("-o", "--outfile", help="Name of output file. Default: mismatches.npy", default='mismatches.npy')
args = parser.parse_args()

min_len = sys.maxsize  
max_len = -1 

len_sum = 0
med_len = 0
lens = []

with open(args.file) as f:
    u = File.UndoHandle(f)
    firstLine = u.peekline()
    if firstLine[0] == '@':
        p = SeqIO.parse(args.file, 'fastq')
    elif firstLine[0] == '>':
        p = SeqIO.parse(args.file, 'fasta')
    else:
        raise Exception('Cannot auto-identify .fasta or .fastq sequence format.')

n = 0
for record in p :
    # process the record...
    l = len(record.seq)
    n = n + 1

    lens.append(l)
    len_sum += l

    if l > max_len:
        max_len = l

    if l < min_len:
        min_len = l

    if args.progress and (n % 10000 == 0):
        print(n)

# Sort lengths to get the median (O(n log n ) instead of O(n), but good for now) 
lens = sorted(lens)
median = lens[n/2]

print(min_len, len_sum/n, median, max_len) 

#for record in SeqIO.parse(args.file):
#   print(record)
